<?php

use Illuminate\Support\Facades\Route;
use App\http\Controllers\userFrontPage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//debug
//debug end


//user
Route::get('/property/{category}', 'UserController@property')->name('property_page');
Route::post('/gallery', 'UserController@galleryFilter')->name('filter_gallery_page');
Route::get('/gallery', 'UserController@gallery')->name('gallery_page');
Route::get('/contact', 'UserController@contact')->name('contact_page');
Route::get('/pricelist', 'UserController@pricelist')->name('pricelist_page');
Route::post('/', 'PricelistController@filterPricelist')->name('filter_pricelist_page');
Route::get('/', 'UserController@index')->name('first_page');
//user end

//admin

//home
Route::post('/admin/home/update/{id}', 'AdminController@update')->name('update_home_page')->middleware('auth');
//home end

//apartment
// Route::delete('/admin/apartment/image/delete/{id}', 'ApartmentController@delete')->name('delete_apartment_page')->middleware('auth');
Route::post('/admin/apartment/image/{id}', 'ApartmentController@dropzoneFileUpload')->name('dropzoneFileUpload_apartment_page')->middleware('auth');
Route::get('/admin/apartment/image/{id}', 'ApartmentController@createImage')->name('createImage_apartment_page')->middleware('auth');
Route::post('/admin/apartment/update/{id}', 'ApartmentController@update')->name('update_apartment_page')->middleware('auth');
Route::get('/admin/apartment/edit/{id}', 'ApartmentController@edit')->name('edit_apartment_page')->middleware('auth');
Route::get('/admin/apartment/show/{id}', 'ApartmentController@show')->name('show_apartment_page')->middleware('auth');
Route::delete('/admin/apartment/destroy/{id}', 'ApartmentController@destroy')->name('destroy_apartment_page')->middleware('auth');
Route::post('/admin/apartment/create', 'ApartmentController@store')->name('store_apartment_page')->middleware('auth');
Route::get('/admin/apartment/create', 'ApartmentController@create')->name('create_apartment_page')->middleware('auth');
Route::get('/admin/apartment', 'ApartmentController@index')->name('apartment_page')->middleware('auth');
//apartment end

//commercial
Route::post('/admin/commercial/image/{id}', 'CommercialController@dropzoneFileUpload')->name('dropzoneFileUpload_commercial_page')->middleware('auth');
Route::get('/admin/commercial/image/{id}', 'CommercialController@createImage')->name('createImage_commercial_page')->middleware('auth');
Route::post('/admin/commercial/update/{id}', 'CommercialController@update')->name('update_commercial_page')->middleware('auth');
Route::get('/admin/commercial/edit/{id}', 'CommercialController@edit')->name('edit_commercial_page')->middleware('auth');
Route::get('/admin/commercial/show/{id}', 'CommercialController@show')->name('show_commercial_page')->middleware('auth');
Route::delete('/admin/commercial/destroy/{id}', 'CommercialController@destroy')->name('destroy_commercial_page')->middleware('auth');
Route::post('/admin/commercial/create', 'CommercialController@store')->name('store_commercial_page')->middleware('auth');
Route::get('/admin/commercial/create', 'CommercialController@create')->name('create_commercial_page')->middleware('auth');
Route::get('/admin/commercial', 'CommercialController@index')->name('commercial_page')->middleware('auth');
//commercial end

//residence
Route::post('/admin/residence/image/{id}', 'ResidenceController@dropzoneFileUpload')->name('dropzoneFileUpload_residence_page')->middleware('auth');
Route::get('/admin/residence/image/{id}', 'ResidenceController@createImage')->name('createImage_residence_page')->middleware('auth');
Route::post('/admin/residence/update/{id}', 'ResidenceController@update')->name('update_residence_page')->middleware('auth');
Route::get('/admin/residence/edit/{id}', 'ResidenceController@edit')->name('edit_residence_page')->middleware('auth');
Route::get('/admin/residence/show/{id}', 'ResidenceController@show')->name('show_residence_page')->middleware('auth');
Route::delete('/admin/residence/destroy/{id}', 'ResidenceController@destroy')->name('destroy_residence_page')->middleware('auth');
Route::post('/admin/residence/create', 'ResidenceController@store')->name('store_residence_page')->middleware('auth');
Route::get('/admin/residence/create', 'ResidenceController@create')->name('create_residence_page')->middleware('auth');
Route::get('/admin/residence', 'ResidenceController@index')->name('residence_page')->middleware('auth');
//residence end

//condotel
Route::post('/admin/condotel/image/{id}', 'CondotelController@dropzoneFileUpload')->name('dropzoneFileUpload_condotel_page')->middleware('auth');
Route::get('/admin/condotel/image/{id}', 'CondotelController@createImage')->name('createImage_condotel_page')->middleware('auth');
Route::post('/admin/condotel/update/{id}', 'CondotelController@update')->name('update_condotel_page')->middleware('auth');
Route::get('/admin/condotel/edit/{id}', 'CondotelController@edit')->name('edit_condotel_page')->middleware('auth');
Route::get('/admin/condotel/show/{id}', 'CondotelController@show')->name('show_condotel_page')->middleware('auth');
Route::delete('/admin/condotel/destroy/{id}', 'CondotelController@destroy')->name('destroy_condotel_page')->middleware('auth');
Route::post('/admin/condotel/create', 'CondotelController@store')->name('store_condotel_page')->middleware('auth');
Route::get('/admin/condotel/create', 'CondotelController@create')->name('create_condotel_page')->middleware('auth');
Route::get('/admin/condotel', 'CondotelController@index')->name('condotel_page')->middleware('auth');
//condotel end

//land
Route::post('/admin/land/image/{id}', 'LandController@dropzoneFileUpload')->name('dropzoneFileUpload_land_page')->middleware('auth');
Route::get('/admin/land/image/{id}', 'LandController@createImage')->name('createImage_land_page')->middleware('auth');
Route::post('/admin/land/update/{id}', 'LandController@update')->name('update_land_page')->middleware('auth');
Route::get('/admin/land/edit/{id}', 'LandController@edit')->name('edit_land_page')->middleware('auth');
Route::get('/admin/land/show/{id}', 'LandController@show')->name('show_land_page')->middleware('auth');
Route::delete('/admin/land/destroy/{id}', 'LandController@destroy')->name('destroy_land_page')->middleware('auth');
Route::post('/admin/land/create', 'LandController@store')->name('store_land_page')->middleware('auth');
Route::get('/admin/land/create', 'LandController@create')->name('create_land_page')->middleware('auth');
Route::get('/admin/land', 'LandController@index')->name('land_page')->middleware('auth');
//land end

//gellery
Route::delete('/admin/gallery/destroy/{id}', 'GalleryController@destroy')->name('destroy_gallery_page')->middleware('auth');
Route::get('/admin/gallery', 'GalleryController@index')->name('gallery_page')->middleware('auth');
//gellery end

Route::post('/admin/contact', 'ContactController@update')->name('update_contact_page')->middleware('auth');
Route::get('/admin/contact', 'ContactController@index')->name('contact_page')->middleware('auth');
Route::post('/admin/store', 'AdminController@store')->name('store_page')->middleware('auth');
Route::get('/admin/create', 'AdminController@create')->name('upload_page')->middleware('auth');
Route::get('/admin', 'AdminController@index')->name('home_admin_page')->middleware('auth');
Route::post('/logout', 'LoginController@logout')->name('logout');
Route::post('/login', 'LoginController@authenticate')->name('login_page');
Route::get('/login', 'LoginController@index')->name('login_page');
//admin end

//middleware
// ->middleware('auth');
//middleware
