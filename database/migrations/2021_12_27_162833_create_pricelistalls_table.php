<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricelistallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricelistalls', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('property_id');
            $table->string('unit');
            $table->string('nett');
            $table->string('sg');
            $table->string('type_unit');
            $table->string('pricelist');
            $table->string('dp_30');
            $table->string('plafond_kpa');
            $table->string('type_offer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricelistalls');
    }
}
