<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricelistteperassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricelistteperass', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('property_id');
            $table->string('blok');
            $table->string('kav');
            $table->string('luas_m2');
            $table->string('pricelist');
            $table->string('type_offer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricelistteperass');
    }
}
