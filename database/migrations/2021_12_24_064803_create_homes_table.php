<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->id();
            $table->longtext('welcome')->nullable();
            $table->integer('apartment_id')->nullable();
            $table->integer('commercial_id')->nullable();
            $table->integer('residence_id')->nullable();
            $table->integer('condotel_id')->nullable();
            $table->integer('land_id')->nullable();
            $table->longtext('about_apartment')->nullable();
            $table->longtext('about_commercial')->nullable();
            $table->longtext('about_residence')->nullable();
            $table->longtext('about_condotel')->nullable();
            $table->longtext('about_land')->nullable();
            $table->string('image_apartment')->nullable();
            $table->string('image_commercial')->nullable();
            $table->string('image_residence')->nullable();
            $table->string('image_condotel')->nullable();
            $table->string('image_land')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
