<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $data = array(
            'name' => 'admin',
            'email' => 'admin123@gmail.com',
            'password' => 'admin123'
        );
        $password = $data['password'];
        $data['password']=Hash::make($data['password']);
        $user = User::create($data);
        // Auth::login($user);
    }
}
