<?php

namespace App\Http\Controllers;

use App\Models\Pricelist;
use App\Models\Pricelistall;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Models\Home;
use App\Models\Contact;
use Illuminate\Pagination\Paginator;

class PricelistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function show(Pricelist $pricelist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function edit(Pricelist $pricelist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pricelist $pricelist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pricelist $pricelist)
    {
        //
    }

    public function filterPricelist(Request $request)
    {
        // dd($request->all());
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        $home = Home::where('id', '=', 1)->first();
        if($home == null){
            $home = '';
        }
        $galleries = Gallery::paginate(6);
        if($request->city && $request->category){
            $properties = Property::where('location', $request->city)
                                    ->where('category_building', $request->category)
                                    ->get();
                                    // dd($properties);
            if(count($properties)>0){
                if($request->offer){
                    foreach($properties as $property){
                        $pricelistes[] = Pricelistall::where('property_id', '=', $property->id)
                                                    ->where('type_offer', $request->offer)
                                                    ->get();
                    }
                    if(count($pricelistes)<1){
                        $allPricelists = [];
                    }else{
                        foreach($pricelistes as $pricelists){
                            foreach($pricelists as $pricelist){
                                $allPricelists[] = $pricelist;
                            }
                        }
                    }
                    // dd($allPricelists);
                    return view('/pricelist/pricelist', ['allPricelists'=>$allPricelists, 'home'=>$home, 'galleries'=>$galleries, 'contact'=>$contact, 'properties'=>$properties]);
                }else{
                    foreach($properties as $property){
                        $pricelistes = Pricelistall::where('property_id', '=', $property->id)
                        ->get();
                    }
                    return view('/pricelist/pricelist', ['pricelistes'=>$pricelistes,'home'=>$home, 'galleries'=>$galleries, 'contact'=>$contact]);
                }
            }else{
                $properties = [];
                $allPricelists = [];
                return view('/pricelist/pricelist', ['allPricelists'=>$allPricelists, 'home'=>$home, 'galleries'=>$galleries, 'contact'=>$contact, 'properties'=>$properties]);
            }
        }
    }
}
