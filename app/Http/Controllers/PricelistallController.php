<?php

namespace App\Http\Controllers;

use App\Models\Pricelistall;
use Illuminate\Http\Request;

class PricelistallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pricelistall  $pricelistall
     * @return \Illuminate\Http\Response
     */
    public function show(Pricelistall $pricelistall)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pricelistall  $pricelistall
     * @return \Illuminate\Http\Response
     */
    public function edit(Pricelistall $pricelistall)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pricelistall  $pricelistall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pricelistall $pricelistall)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pricelistall  $pricelistall
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pricelistall $pricelistall)
    {
        //
    }
}
