<?php

namespace App\Http\Controllers;

use App\Models\Residence;
use App\Models\Gallery;
use App\Models\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class ResidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $residences = Property::where('category_building', 'residence')->get();
        $galleries =  [];
        if($residences){
            foreach($residences as $residence){
                $galleries[] =  Gallery::where('property_id', '=', $residence->id)->first();
            }
        }
        // dd($gallery);
        if($residences && $galleries){
            return view('/admin/property/residence', ['residences'=>$residences, 'galleries'=>$galleries]);
        }
        return view('/admin/property/residence', ['residences' => $residences]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/crud/residence/upload_residence');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
            "image_building" => 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
        ]);
        $originalName = $validatedData['image_building']->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $validatedData['image_building'] = $request->file('image_building')->storeAs('public/images',$uniqImageName);
        $validatedData['image_building'] = 'images/'.$uniqImageName;
        $createResidence = new Property;
        $createResidence->name_building = $validatedData['name_building'];
        $createResidence->location = $validatedData['location'];
        $createResidence->detail_building = $validatedData['detail_building'];
        $createResidence->category_building = $validatedData['category_building'];
        $createResidence->save();
        if($createResidence){
            $addToGallery = new Gallery;
            $addToGallery->property_id = $createResidence->id;
            $addToGallery->image_name = $validatedData['image_building'];
            $addToGallery->save();
        }
        return redirect('/admin/residence')->with('success', 'Berhasil Menambahkan Data Baru!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $residence = Property::find($id);
        $galleries =  Gallery::where('property_id', '=', $id)->get();
        // dd($galleries);
        if($residence && $galleries){
            return view('/admin/crud/residence/detail_residence', ['residence'=>$residence, 'galleries'=>$galleries]);
        }
        if($residence){
            return view('/admin/crud/residence/detail_residence', ['residence'=>$residence]);
        }
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $residence = Property::find($id);
        if($residence){
            return view('/admin/crud/residence/edit_residence', ['residence'=>$residence]);
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255'
        ]);
        // $dataResidence = Property::where('id', $id)->first();
        // if($dataResidence->image_building){
        //     $validatedData['image_building'] = $dataResidence->image_building;
        // }else{
        //     $validatedData['image_building'] = null;
        // }
        $updateGame = Property::where('id', $id)
                        ->update($validatedData);
        return redirect('/admin/residence')->with('success', 'Data has been updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Residence  $residence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $dataResidence = Property::find($id);
        $dataGalleryResidences = Gallery::where('property_id','=',$id)->get();
        foreach($dataGalleryResidences as $dataGalleryResidence){
            // dd($dataGalleryResidence->image_name);
            $deleteImage = Storage::delete('public/'.$dataGalleryResidence->image_name);
            $dataGalleryResidence->delete();
        }
        $dataResidence->delete();
        return redirect('/admin/residence')->with('success', 'Data has been deleted!!!');
    }
    public function createImage(Request $request, $id)
    {
        $residence = Property::where('id', '=', $id)->first();
        return view('/admin/crud/residence/add_image_residence', ['residence'=>$residence]);
    }

    public  function dropzoneFileUpload(Request $request, $id)  
    {  
        $residence = Property::where('id', '=', $id)->first();
        $image = $request->file('file');
        $originalName = $image->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $finalName = 'images/'.$uniqImageName;
        $saveImage = $image->move(public_path('storage/images'),$finalName);
        if($saveImage){
            $addImage = new Gallery;
            $addImage->property_id = $id;
            $addImage->image_name = $finalName;
            $addImage->save();
            return response()->json(['success'=>$finalName]);
        }else{
            return abort('error');
        }
    }
}
