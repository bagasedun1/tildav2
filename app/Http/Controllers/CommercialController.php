<?php

namespace App\Http\Controllers;

use App\Models\Commercial;
use App\Models\Gallery;
use App\Models\Apartment;
use App\Models\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class CommercialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commercials = Property::where('category_building', 'Commercial')->get();
        $galleries =  [];
        if($commercials){
            foreach($commercials as $commercial){
                $galleries[] =  Gallery::where('property_id', '=', $commercial->id)->first();
            }
        }
        // dd($gallery);
        if($commercials && $galleries){
            return view('/admin/property/commercial', ['commercials'=>$commercials, 'galleries'=>$galleries]);
        }
        return view('/admin/property/commercial', ['commercials'=>$commercials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/crud/commercial/upload_commercial');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
            "image_building" => 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
        ]);
        // dd($validatedData);
        $originalName = $validatedData['image_building']->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $validatedData['image_building'] = $request->file('image_building')->storeAs('public/images',$uniqImageName);
        $validatedData['image_building'] = 'images/'.$uniqImageName;
        $createCommercial = new Property;
        $createCommercial->name_building = $validatedData['name_building'];
        $createCommercial->location = $validatedData['location'];
        $createCommercial->detail_building = $validatedData['detail_building'];
        $createCommercial->category_building = $validatedData['category_building'];
        $createCommercial->save();
        if($createCommercial){
            $addToGallery = new Gallery;
            $addToGallery->property_id = $createCommercial->id;
            $addToGallery->image_name = $validatedData['image_building'];
            $addToGallery->save();
        }
        return redirect('/admin/commercial')->with('success', 'Berhasil Menambahkan Data Baru!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commercial  $commercial
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $commercial = Property::find($id);
        $galleries =  Gallery::where('property_id', '=', $id)->get();
        // dd($galleries);
        if($commercial && $galleries){
            return view('/admin/crud/commercial/detail_commercial', ['commercial'=>$commercial, 'galleries'=>$galleries]);
        }
        if($commercial){
            return view('/admin/crud/commercial/detail_commercial', ['commercial'=>$commercial]);
        }
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commercial  $commercial
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $commercial = Property::find($id);
        if($commercial){
            return view('/admin/crud/commercial/edit_commercial', ['commercial'=>$commercial]);
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commercial  $commercial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255'
        ]);

        $updateGame = Property::where('id', $id)
                        ->update($validatedData);
        return redirect('/admin/commercial')->with('success', 'Data has been updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commercial  $commercial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $dataCommercial = Property::find($id);
        $dataGalleryCommercials = Gallery::where('property_id','=',$id)->get();
        foreach($dataGalleryCommercials as $dataGalleryCommercial){
            // dd($dataGalleryCommercial->image_name);
            $deleteImage = Storage::delete('public/'.$dataGalleryCommercial->image_name);
            $dataGalleryCommercial->delete();
        }
        $dataCommercial->delete();
        return redirect('/admin/commercial')->with('success', 'Data has been deleted!!!');
    }

    public function createImage(Request $request, $id)
    {
        // dd($id);
        $commercial = Property::where('id', '=', $id)->first();
        return view('/admin/crud/commercial/add_image_commercial', ['commercial'=>$commercial]);
    }

    public  function dropzoneFileUpload(Request $request, $id)  
    {  
        $commercial = Property::where('id', '=', $id)->first();
        $image = $request->file('file');
        $originalName = $image->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $finalName = 'images/'.$uniqImageName;
        $saveImage = $image->move(public_path('storage/images'),$finalName);
        if($saveImage){
            $addImage = new Gallery;
            $addImage->property_id = $id;
            $addImage->image_name = $finalName;
            $addImage->save();
            return response()->json(['success'=>$finalName]);
        }else{
            return abort('error');
        }
    }
}
