<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;
use App\Models\Gallery;
use App\Models\Home;
use App\Models\Property;
use App\Models\Contact;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        $home = Home::where('id', '=', 1)->first();
        if($home == null){
            $home = '';
        }
        $galleries = Gallery::paginate(6);
        return view('FrontPage/index', ['home'=>$home, 'galleries'=>$galleries, 'contact'=>$contact]);
    }
    
    public function property($category)
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        $properties = Property::where('category_building', '=', $category)->get();
        if(count($properties)<1){
            $properties = '';
            $galleries = [];
        }else{
            foreach($properties as $property){
                $galleries[] = Gallery::where('property_id', '=', $property->id)
                ->get();
            }
        }
        return view('Property/property', ['properties'=>$properties, 'galleries'=>$galleries, 'contact'=>$contact]);
    }

    public function gallery() 
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        $galleries = Gallery::all();
        if(count($galleries)<1){
            $galleries = [];
        }
        return view('gallery/gallery', ['galleries'=>$galleries, 'contact'=>$contact]);
    }

    public function contact() 
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        return view('contact/contact', ['contact'=>$contact]);
    }

    public function footerContact() 
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        return view('component/contact', ['contact'=>$contact]);
    }

    public function pricelist(Type $var = null)
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        $home = Home::where('id', '=', 1)->first();
        if($home == null){
            $home = '';
        }
        $galleries = Gallery::paginate(6);
        return view('debug/pricelist', ['home'=>$home, 'galleries'=>$galleries, 'contact'=>$contact]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function galleryFilter(Request $request)
    {
        $contact = Contact::first();
        if($contact==null){
            $contact = '';
        }
        if($request->filter == 'all'){
            $galleries = Gallery::all();
            if(count($galleries)<1){
                $galleries = [];
            }
            return view('gallery/gallery', ['galleries'=>$galleries, 'contact'=>$contact]);
        }else{
            $properties = Property::where('category_building', '=', $request->filter)
                                ->get();
            if(count($properties)>0){
                foreach($properties as $property){
                    $galleries = Gallery::where('property_id', '=', $property->id)
                                        ->get();
                }
                return view('gallery/gallery', ['galleries'=>$galleries, 'contact'=>$contact]);
            }else{
                $galleries = [];
                return view('gallery/gallery', ['galleries'=>$galleries, 'contact'=>$contact]);
            }
        }
    }
}
