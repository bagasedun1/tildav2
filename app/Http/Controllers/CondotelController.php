<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class CondotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $condotels = Property::where('category_building', 'Condotel')->get();
        $galleries =  [];
        if($condotels){
            foreach($condotels as $condotel){
                $galleries[] =  Gallery::where('property_id', '=', $condotel->id)->first();
            }
        }
        if($condotels && $galleries){
            return view('/admin/property/condotel', ['condotels'=>$condotels, 'galleries'=>$galleries]);
        }
        return view('/admin/property/condotel', ['condotels' => $condotels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $condotel = Property::find($id);
        $galleries =  Gallery::where('property_id', '=', $id)->get();
        // dd($galleries);
        if($condotel && $galleries){
            return view('/admin/crud/condotel/detail_condotel', ['condotel'=>$condotel, 'galleries'=>$galleries]);
        }
        if($condotel){
            return view('/admin/crud/condotel/detail_condotel', ['condotel'=>$condotel]);
        }
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $condotel = Property::find($id);
        if($condotel){
            return view('/admin/crud/condotel/edit_condotel', ['condotel'=>$condotel]);
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
        ]);
        $updateGame = Property::where('id', $id)
                        ->update($validatedData);
        return redirect('/admin/condotel')->with('success', 'Data has been updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataCondotel = Property::find($id);
        $dataGalleryCondotels = Gallery::where('property_id','=',$id)->get();
        foreach($dataGalleryCondotels as $dataGalleryCondotel){
            // dd($dataGalleryCondotel->image_name);
            $deleteImage = Storage::delete('public/'.$dataGalleryCondotel->image_name);
            $dataGalleryCondotel->delete();
        }
        $dataCondotel->delete();
        return redirect('/admin/condotel')->with('success', 'Data has been deleted!!!');
    }
    public function createImage(Request $request, $id)
    {
        // dd($id);
        $condotel = Property::where('id', '=', $id)->first();
        return view('/admin/crud/condotel/add_image_condotel', ['condotel'=>$condotel]);
    }

    public  function dropzoneFileUpload(Request $request, $id)  
    {  
        $condotel = Property::where('id', '=', $id)->first();
        $image = $request->file('file');
        $originalName = $image->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $finalName = 'images/'.$uniqImageName;
        $saveImage = $image->move(public_path('storage/images'),$finalName);
        if($saveImage){
            $addImage = new Gallery;
            $addImage->property_id = $id;
            $addImage->image_name = $finalName;
            $addImage->save();
            return response()->json(['success'=>$finalName]);
        }else{
            return abort('error');
        }
    }
}
