<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class ApartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apartments = Property::where('category_building', 'Apartment')->get();
        $galleries =  [];
        if($apartments){
            foreach($apartments as $apartment){
                $galleries[] =  Gallery::where('property_id', '=', $apartment->id)->first();
            }
        }
        if($apartments && $galleries){
            return view('/admin/property/apartment', ['apartments'=>$apartments, 'galleries'=>$galleries]);
        }
        return view('/admin/property/apartment', ['apartments' => $apartments]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/crud/apartment/upload_apartment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
            "image_building" => 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
        ]);
        $originalName = $validatedData['image_building']->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $validatedData['image_building'] = $request->file('image_building')->storeAs('public/images',$uniqImageName);
        $validatedData['image_building'] = 'images/'.$uniqImageName;
        $createApartment = new Property;
        $createApartment->name_building = $validatedData['name_building'];
        $createApartment->location = $validatedData['location'];
        $createApartment->detail_building = $validatedData['detail_building'];
        $createApartment->category_building = $validatedData['category_building'];
        $createApartment->save();
        if($createApartment){
            $addToGallery = new Gallery;
            $addToGallery->property_id = $createApartment->id;
            $addToGallery->image_name = $validatedData['image_building'];
            $addToGallery->save();
        }
        return redirect('/admin/apartment')->with('success', 'Berhasil Menambahkan Data Baru!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $apartment = Property::find($id);
        $galleries =  Gallery::where('property_id', '=', $id)->get();
        // dd($galleries);
        if($apartment && $galleries){
            return view('/admin/crud/apartment/detail_apartment', ['apartment'=>$apartment, 'galleries'=>$galleries]);
        }
        if($apartment){
            return view('/admin/crud/apartment/detail_apartment', ['apartment'=>$apartment]);
        }
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $apartment = Property::find($id);
        if($apartment){
            return view('/admin/crud/apartment/edit_apartment', ['apartment'=>$apartment]);
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
            // "image_building" => 'required|image|mimes:jpg,png,jpeg,svg|max:15000'
        ]);
        // $dataApartment = Property::where('id', $id)->first();
        // if($dataApartment->image_building){
        //     $validatedData['image_building'] = $dataApartment->image_building;
        // }else{
        //     $validatedData['image_building'] = null;
        // }
        $updateGame = Property::where('id', $id)
                        ->update($validatedData);
        return redirect('/admin/apartment')->with('success', 'Data has been updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $dataApartment = Property::find($id);
        $dataGalleryApartments = Gallery::where('property_id','=',$id)->get();
        foreach($dataGalleryApartments as $dataGalleryApartment){
            // dd($dataGalleryApartment->image_name);
            $deleteImage = Storage::delete('public/'.$dataGalleryApartment->image_name);
            $dataGalleryApartment->delete();
        }
        $dataApartment->delete();
        return redirect('/admin/apartment')->with('success', 'Data has been deleted!!!');
    }

    public function createImage(Request $request, $id)
    {
        // dd($id);
        $apartment = Property::where('id', '=', $id)->first();
        return view('/admin/crud/apartment/add_image_apartment', ['apartment'=>$apartment]);
    }

    public  function dropzoneFileUpload(Request $request, $id)  
    {  
        $apartment = Property::where('id', '=', $id)->first();
        $image = $request->file('file');
        $originalName = $image->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $finalName = 'images/'.$uniqImageName;
        $saveImage = $image->move(public_path('storage/images'),$finalName);
        if($saveImage){
            $addImage = new Gallery;
            $addImage->property_id = $id;
            $addImage->image_name = $finalName;
            $addImage->save();
            return response()->json(['success'=>$finalName]);
        }else{
            return abort('error');
        }
    }
}
