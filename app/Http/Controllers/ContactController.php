<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contact::first();
        return view('/admin/contact', ['contact'=>$contact]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $contact = Contact::where('id', '=', 1)->first();
        if($contact == null){
            $validatedData =$request->validate([
                "email" => 'required|min:8|max:100',
                "address" => 'required|min:6|max:100',
                "phone_number" => 'required|min:8|max:100'
            ]);
            Contact::create($validatedData);
            return redirect('/admin/contact')->with('success', 'Berhasil Merubah Data!!!');
        }else{
            $validatedData =$request->validate([
                "email" => 'required|min:8|max:100',
                "address" => 'required|min:6|max:100',
                "phone_number" => 'required|min:8|max:100'
            ]);
            $updateContact = Contact::where('id', '=', $contact->id)
            ->update($validatedData);
            return redirect('/admin/contact')->with('success', 'Berhasil Merubah Data!!!');
        }
        // dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
