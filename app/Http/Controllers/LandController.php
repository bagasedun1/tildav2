<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lands = Property::where('category_building', 'land')->get();
        $galleries =  [];
        if($lands){
            foreach($lands as $land){
                $galleries[] =  Gallery::where('property_id', '=', $land->id)->first();
            }
        }
        if($lands && $galleries){
            return view('/admin/property/land', ['lands'=>$lands, 'galleries'=>$galleries]);
        }
        return view('/admin/property/land', ['lands' => $lands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $land = Property::find($id);
        $galleries =  Gallery::where('property_id', '=', $id)->get();
        // dd($galleries);
        if($land && $galleries){
            return view('/admin/crud/land/detail_land', ['land'=>$land, 'galleries'=>$galleries]);
        }
        if($land){
            return view('/admin/crud/land/detail_land', ['land'=>$land]);
        }
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $land = Property::find($id);
        if($land){
            return view('/admin/crud/land/edit_land', ['land'=>$land]);
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
        ]);
        $updateGame = Property::where('id', $id)
                        ->update($validatedData);
        return redirect('/admin/land')->with('success', 'Data has been updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataland = Property::find($id);
        $dataGalleryLands = Gallery::where('property_id','=',$id)->get();
        foreach($dataGalleryLands as $dataGalleryland){
            // dd($dataGalleryland->image_name);
            $deleteImage = Storage::delete('public/'.$dataGalleryland->image_name);
            $dataGalleryland->delete();
        }
        $dataland->delete();
        return redirect('/admin/land')->with('success', 'Data has been deleted!!!');
    }
    public function createImage(Request $request, $id)
    {
        // dd($id);
        $land = Property::where('id', '=', $id)->first();
        return view('/admin/crud/land/add_image_land', ['land'=>$land]);
    }

    public  function dropzoneFileUpload(Request $request, $id)  
    {  
        $land = Property::where('id', '=', $id)->first();
        $image = $request->file('file');
        $originalName = $image->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $finalName = 'images/'.$uniqImageName;
        $saveImage = $image->move(public_path('storage/images'),$finalName);
        if($saveImage){
            $addImage = new Gallery;
            $addImage->property_id = $id;
            $addImage->image_name = $finalName;
            $addImage->save();
            return response()->json(['success'=>$finalName]);
        }else{
            return abort('error');
        }
    }
}
