<?php

namespace App\Http\Controllers;

use App\Models\Apartment;
use App\Models\Gallery;
use App\Models\Commercial;
use App\Models\Residence;
use App\Models\Home;
use App\Models\Property;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $apartments = Apartment::where('category_building', '=', 'Apartment')->get();
        // $commercials = Commercial::where('category_building', '=', 'Commercial')->get();
        // $residences = Residence::where('category_building', '=', 'Residence')->get();
        $home = Home::first();
        return view('/admin/index', ['home'=>$home]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/upload/upload_data');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = $request->category_building;
        $validatedData =$request->validate([
            "name_building" => 'required|min:6|max:255',
            "location" => 'required|min:3|max:255',
            "detail_building" => 'required|max:1000000',
            "category_building" => 'required|max:255',
            "image_building" => 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
        ]);
        $originalName = $validatedData['image_building']->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $validatedData['image_building'] = $request->file('image_building')->storeAs('public/images',$uniqImageName);
        $validatedData['image_building'] = 'images/'.$uniqImageName;
        $createApartment = new Property;
        $createApartment->name_building = $validatedData['name_building'];
        $createApartment->location = $validatedData['location'];
        $createApartment->detail_building = $validatedData['detail_building'];
        $createApartment->category_building = $validatedData['category_building'];
        $createApartment->save();
        if($createApartment){
            $addToGallery = new Gallery;
            $addToGallery->property_id = $createApartment->id;
            $addToGallery->image_name = $validatedData['image_building'];
            $addToGallery->save();
        }
        if($category == "Apartment"){
            return redirect('/admin/apartment')->with('success', 'Berhasil Menambahkan Data Baru!!!');
        }elseif($category == "Commercial"){
            return redirect('/admin/commercial')->with('success', 'Berhasil Menambahkan Data Baru!!!');
        }elseif($category == "Residence"){
            return redirect('/admin/residence')->with('success', 'Berhasil Menambahkan Data Baru!!!');
        }elseif($category == "Condotel"){
            return redirect('/admin/condotel')->with('success', 'Berhasil Menambahkan Data Baru!!!');
        }elseif($category == "Land"){
            return redirect('/admin/land')->with('success', 'Berhasil Menambahkan Data Baru!!!');
        }else{
            return abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Home::where('id', '=', $id)->first();
        if($data==null){
            $validatedData =$request->validate([
                "welcome" => 'required|max:1000000',
                "about_apartment"=> 'required|min:4|max:10000',
                "about_commercial"=> 'required|min:4|max:10000',
                "about_residence"=> 'required|min:4|max:10000',
                "about_condotel"=> 'required|min:4|max:10000',
                "about_land"=> 'required|min:4|max:10000',
                "image_apartment"=> 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_commercial"=> 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_residence"=> 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_condotel"=> 'required|image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_land"=> 'required|image|mimes:jpg,png,jpeg,svg|max:15000'
            ]);
            // dd($validatedData);
            $originalNameApartment = $validatedData['image_apartment']->getClientOriginalName();
            $originalNameCommercial = $validatedData['image_commercial']->getClientOriginalName();
            $originalNameResidence = $validatedData['image_residence']->getClientOriginalName();
            $originalNameCondotel = $validatedData['image_condotel']->getClientOriginalName();
            $originalNameLand = $validatedData['image_land']->getClientOriginalName();
            $timeNow = Carbon::now();
            $formatTime = $timeNow->format('YmdHis');
            $uniqImageNameApartment = $formatTime.$originalNameApartment;
            $uniqImageNameCommercial = $formatTime.$originalNameCommercial;
            $uniqImageNameResidence = $formatTime.$originalNameResidence;
            $uniqImageNameCondotel = $formatTime.$originalNameCondotel;
            $uniqImageNameLand = $formatTime.$originalNameLand;
            $validatedData['image_apartment'] = $request->file('image_apartment')->storeAs('public/images',$uniqImageNameApartment);
            $validatedData['image_commercial'] = $request->file('image_commercial')->storeAs('public/images',$uniqImageNameCommercial);
            $validatedData['image_residence'] = $request->file('image_residence')->storeAs('public/images',$uniqImageNameResidence);
            $validatedData['image_condotel'] = $request->file('image_condotel')->storeAs('public/images',$uniqImageNameCondotel);
            $validatedData['image_land'] = $request->file('image_land')->storeAs('public/images',$uniqImageNameLand);
            $validatedData['image_apartment'] = 'images/'.$uniqImageNameApartment;
            $validatedData['image_commercial'] = 'images/'.$uniqImageNameCommercial;
            $validatedData['image_residence'] = 'images/'.$uniqImageNameResidence;
            $validatedData['image_condotel'] = 'images/'.$uniqImageNameCondotel;
            $validatedData['image_land'] = 'images/'.$uniqImageNameLand;
            $createHomeImage = new Home;
            $createHomeImage->welcome = $validatedData['welcome'];
            $createHomeImage->about_apartment = $validatedData['about_apartment'];
            $createHomeImage->about_commercial = $validatedData['about_commercial'];
            $createHomeImage->about_residence = $validatedData['about_residence'];
            $createHomeImage->about_condotel = $validatedData['about_condotel'];
            $createHomeImage->about_land = $validatedData['about_land'];
            $createHomeImage->image_apartment = $validatedData['image_apartment'];
            $createHomeImage->image_commercial = $validatedData['image_commercial'];
            $createHomeImage->image_residence = $validatedData['image_residence'];
            $createHomeImage->image_condotel = $validatedData['image_condotel'];
            $createHomeImage->image_land = $validatedData['image_land'];
            $createHomeImage->save();
            return redirect('/admin')->with('success', 'Data has changed!!!');
        }else{

            $validatedData =$request->validate([
                "welcome" => 'required|max:1000000',
                "about_apartment"=> 'required|min:4|max:10000',
                "about_commercial"=> 'required|min:4|max:10000',
                "about_residence"=> 'required|min:4|max:10000',
                "about_condotel"=> 'required|min:4|max:10000',
                "about_land"=> 'required|min:4|max:10000',
                "image_apartment"=> 'image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_commercial"=> 'image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_residence"=> 'image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_condotel"=> 'image|mimes:jpg,png,jpeg,svg|max:15000',
                "image_land"=> 'image|mimes:jpg,png,jpeg,svg|max:15000'
            ]);
            // dd($validatedData);
            $imageApartment = $data->image_apartment;
            $imageCommercial = $data->image_commercial;
            $imageResidence = $data->image_residence;
            $imageCondotel = $data->image_condotel;
            $imageLand = $data->image_land;

            if($request->image_apartment){
                if($imageApartment){
                    storage::delete('public/'.$imageApartment);
                }
                $originalNameApartment = $validatedData['image_apartment']->getClientOriginalName();
                $timeNow = Carbon::now();
                $formatTime = $timeNow->format('YmdHis');
                $uniqImageNameApartment = $formatTime.$originalNameApartment;
                $validatedData['image_apartment'] = $request->file('image_apartment')->storeAs('public/images',$uniqImageNameApartment);
                $validatedData['image_apartment'] = 'images/'.$uniqImageNameApartment;
            }else{
                $validatedData['image_apartment'] = $imageApartment;
            }
            if($request->image_commercial){
                if($imageCommercial){
                    storage::delete('public/'.$imageCommercial);
                }
                $originalNameCommercial = $validatedData['image_commercial']->getClientOriginalName();
                $timeNow = Carbon::now();
                $formatTime = $timeNow->format('YmdHis');
                $uniqImageNameCommercial = $formatTime.$originalNameCommercial;
                $validatedData['image_commercial'] = $request->file('image_commercial')->storeAs('public/images',$uniqImageNameCommercial);
                $validatedData['image_commercial'] = 'images/'.$uniqImageNameCommercial;
            }else{
                $validatedData['image_commercial'] = $imageCommercial;
            }
            if($request->image_residence){
                if($imageResidence){
                    storage::delete('public/'.$imageResidence);
                }
                $originalNameResidence = $validatedData['image_residence']->getClientOriginalName();
                $timeNow = Carbon::now();
                $formatTime = $timeNow->format('YmdHis');
                $uniqImageNameResidence = $formatTime.$originalNameResidence;
                $validatedData['image_residence'] = $request->file('image_residence')->storeAs('public/images',$uniqImageNameResidence);
                $validatedData['image_residence'] = 'images/'.$uniqImageNameResidence;
            }else{
                $validatedData['image_residence'] = $imageResidence;
            }
            if($request->image_condotel){
                if($imageCondotel){
                    storage::delete('public/'.$imageCondotel);
                }
                $originalNameCondotel = $validatedData['image_condotel']->getClientOriginalName();
                $timeNow = Carbon::now();
                $formatTime = $timeNow->format('YmdHis');
                $uniqImageNameCondotel = $formatTime.$originalNameCondotel;
                $validatedData['image_condotel'] = $request->file('image_condotel')->storeAs('public/images',$uniqImageNameCondotel);
                $validatedData['image_condotel'] = 'images/'.$uniqImageNameCondotel;
            }else{
                $validatedData['image_condotel'] = $imageCondotel;
            }
            if($request->image_land){
                if($imageLand){
                    storage::delete('public/'.$imageLand);
                }
                $originalNameLand = $validatedData['image_land']->getClientOriginalName();
                $timeNow = Carbon::now();
                $formatTime = $timeNow->format('YmdHis');
                $uniqImageNameLand = $formatTime.$originalNameLand;
                $validatedData['image_land'] = $request->file('image_land')->storeAs('public/images',$uniqImageNameLand);
                $validatedData['image_land'] = 'images/'.$uniqImageNameLand;
            }else{
                $validatedData['image_land'] = $imageLand;
            }
            $updateHome = Home::where('id', $id)
                        ->update($validatedData);
            return redirect('/admin')->with('success', 'Data has changed!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexContact()
    {
        return view('/admin/contact');
    }
}
