<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Home;

class Apartment extends Model
{
    use HasFactory;
    // protected $guarded =['id'];
    protected $fillable = [
    	'name_building',
        'quote',
        'detail_building',
        'category_building',
        'image_building'
    ];

    // public function home()
    // {
    //     return $this->hasMany(Home::class);
    // }
}
