<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    protected $fillable = [
    	'apartment_id',
    	'commercial_id',
    	'residence_id',
    	'image_name'
    ];

    public function apartment()
    {
        return $this->hasMany(Apartment::class);
    }
    // public function apartment()
    // {
    //     return $this->belongsTo(Apartment::class);
    // }
}
