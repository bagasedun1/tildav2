<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pricelist extends Model
{
    use HasFactory;
    protected $fillable = [
        'property_id',
        'unit',
        'nett',
        'sg',
        'type_unit',
        'pricelist',
        'dp_30%',
        'plafond_kpa',
        'type_offer',
    ];
    
    public function property()
    {
        return $this->belongsTo(Property::class);
    }
}
