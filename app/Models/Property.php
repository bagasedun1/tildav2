<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    protected $guarded =['id'];

    public function pricelist()
    {
        return $this->hasMany(Pricelist::class);
    }
    public function home()
    {
        return $this->hasMany(home::class);
    }
}
