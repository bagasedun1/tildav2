<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Apartment;

class Home extends Model
{
    use HasFactory;
    // protected $guarded =['id'];
    protected $fillable = [
    	'country_name',
        'welcome',
        'apartment_id',
        'commercial_id',
        'residence_id',
        'about_apartment',
        'about_commercial',
        'about_residence',
        'image_apartment',
        'image_commercial',
        'image_residence'
    ];

    

    // public function apartment()
    // {
    //     return $this->hasMany(Apartment::class);
    // }
    public function apartment()
    {
        return $this->belongsTo(Apartment::class);
    }
    public function commercial()
    {
        return $this->belongsTo(Commercial::class);
    }
    public function residence()
    {
        return $this->belongsTo(Residence::class);
    }

}
