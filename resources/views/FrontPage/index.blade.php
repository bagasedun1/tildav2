<!DOCTYPE html>
<html lang="en">
<head>
    @include('component.head')
</head>
<body>

    @include('component.modal')

    <header>
        @include('component.header')
    </header>

    <section class="mysection">
        @if($galleries)
        @foreach($galleries as $gallery)
        <div class="branda-container">
            <div class="slide-show">
                <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
            </div>
        </div>
        @endforeach
        @else
        <div class="branda-container">
            <div class="slide-show">
                <img src="..." alt="">
            </div>
        </div>
        @endif

        <div class="branda-search">
            <div class="branda-search-field">
            <form action="/" method="post">
                @csrf
                <input type="text" name="city" list="Location" placeholder="Location" required>
                    <datalist id="Location">
                        <option value="Bogor">
                        <option value="Bandung">
                        <option value="Jakarta">
                        <option value="Jogja">
                        <option value="Magelang">
                        <option value="Malang">
                        <option value="Salatiga">
                        <option value="Semarang">
                        <option value="Surabaya">
                    </datalist>

                <input type="text" name="category" list="Type" placeholder="Type" required>
                    <datalist id="Type">
                        <option value="Apartment">
                        <option value="Commercial">
                        <option value="Residence">
                        <option value="Condotel">
                        <option value="Land">
                    </datalist>

                <input type="text" name="offer" list="rentbuy" placeholder="Rent/Buy" required>
                    <datalist id="rentbuy">
                        <option value="Rent">
                        <option value="Buy">
                    </datalist>

                <button type="submit">Find</button>
            </form>
            </div>
        </div>

        <a class="prev" onclick="plusSlide(-1)">&#10094;</a>
        <a class="next" onclick="plusSlide(1)">&#10095;</a>

    </section>

    <section class="mysection">
        <div class="about-container">
            <div class="about-text-field">
                <div class="text-head">
                    @if($home)
                    {!!$home->welcome!!}
                    @else
                    <h1>No data found!</h1>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section class="mysection">
        <div class="property-container">
            <div class="property-field">
                <div class="property-head">
                    <h1>Property Type</h1>
                </div>
                @if($home)
                <div class="property-image">    
                    <div class="property-image-holder">
                        <div class="image-building">
                            <img src="{{ asset('storage/'.$home->image_apartment) }}" alt="">
                        </div>
                        <div class="detail-building">
                            <h1>Apartement</h1>
                            <p>{{$home->about_apartment}}</p>
                        </div>
                    </div>
                    <div class="property-image-holder">
                        <div class="image-building">
                            <img src="{{ asset('storage/'.$home->image_residence) }}" alt="">
                        </div>
                        <div class="detail-building">
                            <h1>Residece</h1>
                            <p>{{$home->about_residence}}</p>
                        </div>
                    </div>
                    <div class="property-image-holder">
                        <div class="image-building">
                            <img src="{{ asset('storage/'.$home->image_commercial) }}" alt="">
                        </div>
                        <div class="detail-building">
                            <h1>Commercial</h1>
                            <p>{{$home->about_commercial}}</p>
                        </div>
                    </div>
                    <div class="property-image-holder">
                        <div class="image-building">
                            <img src="{{ asset('storage/'.$home->image_condotel) }}" alt="">
                        </div>
                        <div class="detail-building">
                            <h1>Condotel</h1>
                            <p>{{$home->about_condotel}}</p>
                        </div>
                    </div>
                    <div class="property-image-holder">
                        <div class="image-building">
                            <img src="{{ asset('storage/'.$home->image_land) }}" alt="">
                        </div>
                        <div class="detail-building">
                            <h1>Land</h1>
                            <p>{{$home->about_land}}</p>
                        </div>
                    </div>
                </div>
                @else
                <h1>No data found!</h1>
                @endif
                
            </div>

            <a class="prevx" style="color:black">&#10094;</a>
            <a class="nextx" style="color:black">&#10095;</a>

        </div>
    </section>

    <section class="mysection">
        <div class="gallery-container">
            <div class="gallery-field">
                <div class="gallery-head">
                    <h1>Gallery</h1>
                </div>

                <div class="gallery-image">
                    @if(count($galleries)>'0')
                        @foreach($galleries as $gallery)
                        <div class="gallery-image-holder">
                            <div class="gallery-building">
                                <img src="{{ asset('storage/'.$gallery->image_name) }}" onclick="openModal()" alt="">
                            </div>
                        </div>
                        @endforeach
                    @else
                    <h1>No data found!</h1>

                    @endif

                </div>
                <hr>
            </div>

        </div>
    </section>

    <section class="mysection">
        @include('component.contact')
    </section>

    <footer>
        @include('component.footer')
    </footer>
    
    <script src="{{ asset('js/slideshow.js') }}"></script>
    <script src="{{ asset('js/modal.js') }}"></script>
    <script>
        $('.property-image').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '.prevx',
            nextArrow: '.nextx',
            responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
    </script>
</body>
</html>