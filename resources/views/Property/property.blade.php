<!DOCTYPE html>
<html lang="en">
<head>
    @include('component.head')
</head>
<body>

    <header>
        @include('component.header')
    </header>

    <section class="prosection">
        <div class="property-container">
            <div class="property-field">
                @if($properties)
                @foreach($properties as $property)
                <div class="property-type-text">
                    <h1>{{{$property->category_building}}}</h1>
                    <h2>{{$property->name_building}}</h2>
                </div>
                
                <div class="slidex">

               
                @foreach($galleries as $gallerys)
                    @foreach($gallerys as $gallery)
                    
                        @if($property->category_building == 'Apartment')
                            @if($gallery->property_id == $property->id)
                            <div class="property-img">
                                <div class="property-img-show">
                                    <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
                                </div>
                            </div>
                            @else
                            @endif

                        @elseif ($property->category_building == 'Commercial')
                            @if($gallery->property_id == $property->id)
                            <div class="property-img-show">
                                <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
                            </div>
                            @else
                            @endif

                        @elseif ($property->category_building == 'Residence')
                            @if($gallery->property_id == $property->id)
                            <div class="property-img-show">
                                <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
                            </div>
                            @else
                            @endif

                        @elseif ($property->category_building == 'Condotel')
                            @if($gallery->property_id == $property->id)
                            <div class="property-img-show">
                                <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
                            </div>
                            @else
                            @endif
                            
                        @elseif ($property->category_building == 'Land')
                            @if($gallery->property_id == $property->id)
                            <div class="property-img-show">
                                <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
                            </div>
                            @else
                            @endif
                        @else
                        @endif
                    
                    @endforeach
                @endforeach

                </div>

                <div class="property-detail">

                          
                    <div class="property-text">
                        <h1>Detail</h1>
                    </div>

                    {!!$property->detail_building!!}
                </div>
                @endforeach
                @else
                <h1>No data found!</h1>
                @endif
                
            </div>
        </div>


        
    </section>

    <section class="prosection">
        @include('component.contact')
    </section>

    <footer>
        @include('component.footer')
    </footer>
    
    <script>
        $('.slidex').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '.prevx',
            nextArrow: '.nextx',
            autoplay: true,
            autoplaySpeed: 5000,
            responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
    </script>

</body>
</html>