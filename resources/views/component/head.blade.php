<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.12.1/css/all.css">
<link rel="icon" href="{{ asset('image/tildacopy.png') }}">
<link href="{{ asset('style/style.css') }}" rel="stylesheet">
<link href="{{ asset('style/style-container.css') }}" rel="stylesheet">
<link href="{{ asset('style/style-property.css') }}" rel="stylesheet">
<link href="{{ asset('style/style-gallery.css') }}" rel="stylesheet">
<link href="{{ asset('style/style-contact.css') }}" rel="stylesheet">
<link href="{{ asset('style/style-price.css') }}" rel="stylesheet">
<link href="{{ asset('style/style-form.css') }}" rel="stylesheet">
<link href="{{ asset('style/header.css') }}" rel="stylesheet">
<link href="{{ asset('style/footer.css') }}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<title>Property Asset Investation</title>