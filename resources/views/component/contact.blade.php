<div class="contact-container">
    <div class="contact-field">
        <div class="contact-text">
            <div class="contact-text left">
                <div class="contact-info">
                    <div class="contact-info-text">
                        <h1>Contact Us</h1>
                        @if($contact)
                        <p>{{$contact->phone_number}}</p>
                        <p>{{$contact->email}}</p>
                        <p>{{$contact->address}}</p>
                        @else
                        <p>phone_number</p>
                        <p>email</p>
                        <p>address</p>
                        @endif
                    </div> 
                    
                    <div class="contact-logo">
                        <h1 style="color: white;">Find Us</h1>
                        <ul>
                            <li class="icon"><a href="https://www.facebook.com/aset.wr?_rdc=2&_rdr"><div class="img-space">
                                <i class="fab fa-facebook-f"></i>
                            </div></a></li>
                            <li class="icon"><a href="https://vt.tiktok.com/ZSeeH7bqU/"><div class="img-space">
                                <i class="fas fa-signature"></i>
                            </div></a></li>
                            <li class="icon"><a href="https://www.youtube.com/channel/UCUvfdjrLrui69lyf_fGZPww"><div class="img-space">
                                <i class="fab fa-twitter"></i>
                            </div></a></li>
                            <li class="icon"><a href="https://www.instagram.com/aset.wr/"><div class="img-space">
                                <i class="fab fa-instagram"></i>
                            </div></a></li>
                        </ul>
                    </div>

                    <div class="contact-ss">
                        <div class="contact-icon">
                            <a href="#"><img src="{{ asset('image/WebHomePAI.png') }}" alt=""></a>
                        </div>
                        <div class="contact-icon">
                            <a href="#"><img src="{{ asset('image/OLX_NEW_LOGO.png') }}" alt=""></a>
                        </div>
                        <div class="contact-icon">
                            <a href="#"><img src="{{ asset('image/Panduan-Jual-Beli-Pr.png') }}" alt=""></a>
                        </div>
                    </div>

                </div>    
            </div>
            <div class="contact-text right">
                <div id="googleMap"></div>
            </div>
        </div>
    </div>
</div>