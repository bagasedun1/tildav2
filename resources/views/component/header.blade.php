<div class="header-container">
    <div class="header-container left">
        <img src="{{ asset('image/Logo_Property_Aset_I.png') }}" alt="">
    </div>

    <div class="header-container right">
        <div class="header-menu">
            <ul>
                <li><a href="/">Home</a></li>
                <li>
                    <a href="#">Property</a>
                    <ul>
                        <li><a href="/property/Apartment">Apartement</a></li>
                        <li><a href="/property/Commercial">Commercial</a></li>
                        <li><a href="/property/Residence">Residential</a></li>
                        <li><a href="/property/Condotel">Condotel</a></li>
                        <li><a href="/property/Land">Land</a></li>
                    </ul>
                </li>
                <li><a href="/gallery">Gallery</a></li>
                <li><a href="/contact">Contact Us</a></li>                      
            </ul>
        </div>
    </div>

    <div class="header-container another">
        <div class="header-menu">
            <ul>
                <li class="icon">
                    <a href="https://www.facebook.com/aset.wr?_rdc=2&_rdr">
                        <div class="img-space">
                            <i class="fab fa-facebook-f"></i>
                        </div>
                    </a>
                </li>
                <li class="icon">
                    <a href="https://vt.tiktok.com/ZSeeH7bqU/">
                        <div class="img-space">
                            <i class="fas fa-signature"></i>
                        </div>
                    </a>
                </li>
                <li class="icon">
                    <a href="https://www.youtube.com/channel/UCUvfdjrLrui69lyf_fGZPww">
                        <div class="img-space">
                            <i class="fab fa-twitter"></i>
                        </div>
                    </a>
                </li>
                <li class="icon">
                    <a href="https://www.instagram.com/aset.wr/">
                        <div class="img-space">
                            <i class="fab fa-instagram"></i>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>