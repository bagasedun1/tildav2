<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Admin tildar</h3>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="{{url('/admin')}}">Home</a>
        </li>
        <li class="active">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Property</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li>
                    <a href="{{url('/admin/apartment')}}">Apartment</a>
                </li>
                <li>
                    <a href="{{url('/admin/commercial')}}">Commercial</a>
                </li>
                <li>
                    <a href="{{url('/admin/residence')}}">Residence</a>
                </li>
                <li>
                    <a href="{{url('/admin/condotel')}}">Condotel</a>
                </li>
                <li>
                    <a href="{{url('/admin/land')}}">Land</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{url('/admin/gallery')}}">Gallery</a>
        </li>
        <li>
            <a href="{{url('/admin/contact')}}">Contact</a>
        </li>
        <li>
            <a href="{{url('/admin/create')}}">Add Data</a>
        </li>
    </ul>
</nav>