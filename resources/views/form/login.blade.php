<!DOCTYPE html>
<html lang="en">
<head>
    @include('component.head')
</head>
<body>

    <main>

        <div class="container">

            <img src="{{asset('image/.jpg')}}" alt="Logo">
            
            <div class="container-login">

                <div class="login-body">
                    <h3>Login Page</h3>
                    <!-- alert success --> 
                    @if(session()->has('success'))
                        <div class="alert alert-success" style="color: green;font-weight:bold">
                           
                        </div>
                    @endif
                    <!-- END::Alert success -->
                    <!-- alert error -->
                    @if(session()->has('error'))
                        <div class="alert alert-danger" style="color: red;font-weight:bold; margin-bottom: 10px">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <!-- END::Alert error -->
                    <form method="post" action="/login">
                    @csrf
                    <div class="mb-3">
                            <label for="email" class="form-label">email</label>  <br>
                            <input type="email" class="form-control" id="email" name="email" autofocus required>
                        </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label> <br>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>

                    <!-- <div class="mb-3">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="dropdownCheck">
                            <label class="form-check-label" for="dropdownCheck">
                            Remember me
                            </label>
                        </div>
                    </div> -->

                    <button type="submit" class="btn btn-primary">Sign In</button>

                    <a href="{{ url('register')}}">Don't have any account ?</a>
                    
                    </form>
                </div>
                
            </div>
        
        </div>

    </main>

</body>
</html>