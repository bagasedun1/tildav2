<!DOCTYPE html>
<html lang="en">
<head>
    @include('component.head')
</head>
<body>

    <header>
        @include('component.header')
    </header>

    <section class="galsection">
        <div class="gallery-filter">
            <form action="/gallery" method="post">
                @csrf
                <div class="gallery-box">
                    <input type="radio" id="image" name="filter" checked="" value="all">
                    <label for="image">All Image</label>
                </div>
                <div class="gallery-box">
                    <input type="radio" id="Apartment" name="filter" value="Apartment">
                    <label for="Apartment">Apartment</label>
                </div>
                <div class="gallery-box">
                    <input type="radio" id="Commercial" name="filter" value="Commercial">
                    <label for="Commercial">Commercial</label>
                </div>
                <div class="gallery-box">
                    <input type="radio" id="Residence" name="filter" value="Residence">
                    <label for="Residence">Residence</label>
                </div>
                <div class="gallery-box">
                    <input type="radio" id="Condotel" name="filter" value="Condotel">
                    <label for="Condotel">Condotel</label>
                </div>
                <div class="gallery-box">
                    <input type="radio" id="Land" name="filter" value="Land">
                    <label for="Land">Land</label>
                </div>
                <button type="submit">Filter</button>
            </form>
        </div>
        <div class="gallery-container">
            <div class="gallery-field">
                <div class="gallery-image-container">
                @if($galleries)
                    @foreach($galleries as $gallery)
                    <div class="gallery-image">
                        <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="patah">
                    </div>
                    @endforeach
                @else
                <h1>No data found!</h1>
                @endif
                </div>
            </div>
            <hr>
        </div>
    </section>
    
    <section class="galsection">
        @include('component.contact')
    </section>

    <footer>
        @include('component.footer')
    </footer>
    
    <script src="{{ asset('js/radiojs.js') }}"></script>

</body>
</html>