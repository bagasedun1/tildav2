<!DOCTYPE html>
<html lang="en">
<head>
    @include('component.head')
</head>
<body>

    @include('component.modal')

    <header>
        @include('component.header')
    </header>

    <section class="pricesection">
        @if($galleries)
        @foreach($galleries as $gallery)
        <div class="branda-container">
            <div class="slide-show">
                <img src="{{ asset('storage/'.$gallery->image_name) }}" alt="">
            </div>
        </div>
        @endforeach
        @else
        <div class="branda-container">
            <div class="slide-show">
                <img src="..." alt="">
            </div>
        </div>
        @endif

        <div class="branda-search">
            <div class="branda-search-field">
            <form action="/" method="post">
                @csrf
                <input type="text" name="city" list="Location" placeholder="Location" required>
                    <datalist id="Location">
                        <option value="Jakarta">
                        <option value="Bogor">
                    </datalist>

                <input type="text" name="category" list="Type" placeholder="Type" required>
                    <datalist id="Type">
                        <option value="Apartment">
                        <option value="Commercial">
                        <option value="Residence">
                    </datalist>

                <input type="text" name="offer" list="rentbuy" placeholder="Rent/Buy" required>
                    <datalist id="rentbuy">
                        <option value="Rent">
                        <option value="Buy">
                    </datalist>

                <button type="submit">Find</button>
            </form>
            </div>
        </div>

        <a class="prev" onclick="plusSlide(-1)">&#10094;</a>
        <a class="next" onclick="plusSlide(1)">&#10095;</a>

    </section>

    <section class="pricesection">
        @if(count($allPricelists)>0)
        <div class="table-price">
        @foreach($properties as $property)
            <h2 for="">{{$property->name_building}}</h2>
            <table>
                <tr>
                    <th>No</th>
                    <th>Lantai/Unit</th>
                    <th>Nett</th>
                    <th>SG</th>
                    <th>Tipe Unit</th>
                    <th>Pricelist</th>
                    <th>DP 30%</th>
                    <th>Plafont KPA</th>
                </tr>
            <?php $no = 0;?>
            @foreach($allPricelists as $allPricelist)
            <?php $no++;?>
                @if($allPricelist->property_id == $property->id)
                <tr>
                    <td>{{$no}}</td>
                    <td>{{$allPricelist->unit}}</td>
                    <td>{{$allPricelist->nett}}</td>
                    <td>{{$allPricelist->sg}}</td>
                    <td>{{$allPricelist->type_unit}}</td>
                    <td>{{$allPricelist->pricelist}}</td>
                    <td>{{$allPricelist->dp_30}}</td>
                    <td>{{$allPricelist->plafond_kpa}}</td>
                </tr>
                @else
                @endif
            @endforeach
            </table>
        @endforeach
        
        </div>
        @else
        <h1>No data found!</h1>
        @endif
    </section>
    
    <section class="pricesection">
        @include('component.contact')
    </section>
    
    <footer>
        <script src="{{ asset('js/slideshow.js') }}"></script>
        <script src="{{ asset('js/modal.js') }}"></script>
        @include('component.footer')
    </footer>
    
    <script>
        $('.property-image').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '.prevx',
            nextArrow: '.nextx',
            responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
    </script>
</body>
</html>