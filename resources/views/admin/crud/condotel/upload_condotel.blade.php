<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Scrollbar Custom CSS -->
    <link href="{{ asset('style/sidebar/sidebar.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css"> -->
    
    <!-- ckeditor -->
    <link href="{{ asset('ckeditor/plugins/codesnippet/lib/highlight/styles/default.css') }}" rel="stylesheet">

    <!-- dropzone -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script> -->


    <title>Document</title>
</head>
<body>
    <main>
        <div class="wrapper">
            <!-- Sidebar -->
            <nav id="sidebar">
            @include('component.sidebar')
            </nav>
            <!-- Page Content -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <i class="fas fa-align-left"></i>
                            <span>Toggle Sidebar</span>
                        </button>
                        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-align-justify"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="nav navbar-nav ml-auto">
                                <li class="mt-1">
                                    <a href="{{url('/admin')}}">
                                        <span>
                                            home
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16">
                                                <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/>
                                            </svg>
                                        </span>
                                    </a>
                                </li> 
                                <li class="ml-3">
                                    <p>Tilda Apart</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <form action="/admin/condotel/create" enctype = "multipart/form-data" method="post">
                    @csrf
                    <div class="container">
                        <label for="building_name" class="font-weight-bold" >Building Name    :</label>
                        <input class="form-control" type="text" placeholder="Building Name" name="name_building" required value="{{ old('name_building') }}">
                            @error('name_building')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        <br>
                        <label for="location" class="font-weight-bold" >Location    :</label>
                        <select class="form-control" name="location">
                            <option value="Bogor">Bogor</option>
                            <option value="Bandung">Bandung</option>
                            <option value="Jakarta">Jakarta</option>
                            <option value="Jogja">Jogja</option>
                            <option value="Magelang">Magelang</option>
                            <option value="Malang">Malang</option>
                            <option value="Salatiga">Salatiga</option>
                            <option value="Semarang">Semarang</option>
                            <option value="Surabaya">Surabaya</option>
                        </select>
                        <label for="building_detail" class="font-weight-bold">Building Detail    :</label>
                        <textarea id="konten" class="form-control" name="detail_building" rows="10" cols="50" >{{ old('detail_building') }}</textarea>
                            @error('detail_building')
                                <p class="text-danger">{{ $message }}</p>
                            @enderror
                        <br>
                        <label for="building_detail" class="font-weight-bold">Building Category  :</label>
                        <select class="form-control" name="category_building">
                            <option value="condotel">condotel</option>
                        </select>
                        <br>
                        <div>
                            <label for="building_detail" class="font-weight-bold">Building Image  :</label>
                            <br>
                            <input type="file" name="image_building">
                        </div>
                    </div>
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                            <span class="btn btn-danger"><a href="{{ URL::previous() }}">Cancel</a></span>
                            <button class="btn btn-info ml-1" type="submit">Upload</button>
                        </div>
                </form>
            </div>
        </div>   
    </main>
    
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- ckeditor -->
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script>
    var konten = document.getElementById("konten");
        CKEDITOR.replace(konten,{
        language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
    </script>

    <script src="{{ asset('ckeditor/plugins/codesnippet/lib/highlight/highlight.pack.js') }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <!-- ckeditor end -->

    <script type="text/javascript">
        $(document).ready(function(){
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
</html>