<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Scrollbar Custom CSS -->
    <link href="{{ asset('style/sidebar/sidebar.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css"> -->

    <!-- ckeditor -->
    <link href="{{ asset('ckeditor/plugins/codesnippet/lib/highlight/styles/default.css') }}" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <main>
    <div class="wrapper">

    <!-- Sidebar -->
    <nav id="sidebar">
    @include('component.sidebar')
    </nav>

    <!-- Page Content -->
    <div id="content">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Toggle Sidebar</span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="mt-1">
                            <a href="{{url('/admin')}}">
                                <span>
                                    home
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16">
                                        <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/>
                                    </svg>
                                </span>
                            </a>
                        </li>
                        
                        <li class="ml-3">
                            <p>Tilda Apart</p>
                        </li>
                        <form action="/logout" method="post">
                            @csrf
                            <button type="submit">
                                Sign Out
                            </button>
                        </form>

                    </ul>
                </div>
            </div>
        </nav>
        <!-- isi content -->
        <!-- alert success --> 
        @if(!empty($success))
            <div class="alert alert-success" id="modalAlert">
                {{ $success }}
            </div>
            @elseif(session()->has('success'))
            <div class="alert alert-success" id="modalAlert">
                {{ session()->get('success') }}
            </div>
            @endif
            <!-- END::Alert success -->
            <br>
            <form action="/admin/home/update/1" enctype = "multipart/form-data" method="post">
                @csrf
                <div class="container">
                    <h1>Home Page Setting</h1> <br>
                    
                    <br>
                    <label for="about" class="font-weight-bold">About   :</label>
                    @if($home != null)
                    <textarea id="konten" class="form-control" name="welcome" rows="10000" cols="50000" >{{ old('about', $home->welcome) }}</textarea>
                    @else
                    <textarea id="konten" class="form-control" name="welcome" rows="10000" cols="50000" >{{ old('about') }}</textarea>
                    @endif
                        @error('welcome')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    <br>
                    <div>
                        <label for="image_apartment" class="font-weight-bold">Apartment Image  :</label>
                        <br>
                        @if($home != null)
                        <img src="{{ asset('storage/'.$home->image_apartment) }}" class="img-thumbnail" alt="..." style="width: 150px;height: 150px;margin-bottom: 10px;">
                        @else
                        <img src="..." class="img-thumbnail" alt="..." style="width: 150px;height: 150px;margin-bottom: 10px;">
                        @endif
                        <br>
                        <input type="file" name="image_apartment">
                        @error('image_apartment')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <br>
                    <label for="about_apartment" class="font-weight-bold" >About Apartment   :</label>
                    @if($home != null)
                    <input class="form-control" type="text" placeholder="About Apartment" name="about_apartment" required value="{{ old('about_apartment', $home->about_apartment) }}">
                    @else
                    <input class="form-control" type="text" placeholder="About Apartment" name="about_apartment" required value="{{ old('about_apartment') }}">
                    @endif
                        @error('about_apartment')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    <br>
                    <div>
                        <label for="image_commercial" class="font-weight-bold">Commercial Image  :</label>
                        <br>
                        @if($home != null)
                        <img src="{{ asset('storage/'.$home->image_commercial) }}" class="img-thumbnail" alt="..." style="width: 150px;height: 150px;margin-bottom: 10px;">
                        @else
                        <img src="..." class="img-thumbnail" alt="..." style="width: 150px;height: 150px;margin-bottom: 10px;">
                        @endif
                        <br>
                        <input type="file" name="image_commercial">
                        @error('image_commercial')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <br>
                    <label for="about_commercial" class="font-weight-bold" >About Commercial   :</label>
                    @if($home != null)
                    <input class="form-control" type="text" placeholder="About Commercial" name="about_commercial" required value="{{ old('about_commercial', $home->about_commercial) }}">
                    @else
                    <input class="form-control" type="text" placeholder="About Commercial" name="about_commercial" required value="{{ old('about_commercial') }}">
                    @endif
                        @error('about_commercial')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    <br>
                    <div>
                        <label for="image_residence" class="font-weight-bold">Residence Image  :</label>
                        <br>
                        @if($home != null)
                        <img src="{{ asset('storage/'.$home->image_residence) }}" class="img-thumbnail" alt="..." style="width: 150px;height: 150px">
                        @else
                        <img src="..." class="img-thumbnail" alt="..." style="width: 150px;height: 150px">
                        @endif
                        <br>
                        <input type="file" name="image_residence">
                        @error('image_residence')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <br>
                    <label for="about_residence" class="font-weight-bold" >About Residence   :</label>
                    @if($home != null)
                    <input class="form-control" type="text" placeholder="About Residence" name="about_residence" required value="{{ old('about_residence', $home->about_residence) }}">
                    @else
                    <input class="form-control" type="text" placeholder="About Residence" name="about_residence" required value="{{ old('about_residence') }}">
                    @endif
                        @error('about_residence')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    <br>
                    <div>
                        <label for="image_condotel" class="font-weight-bold">condotel Image  :</label>
                        <br>
                        @if($home != null)
                        <img src="{{ asset('storage/'.$home->image_condotel) }}" class="img-thumbnail" alt="..." style="width: 150px;height: 150px">
                        @else
                        <img src="..." class="img-thumbnail" alt="..." style="width: 150px;height: 150px">
                        @endif
                        <br>
                        <input type="file" name="image_condotel">
                        @error('image_condotel')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <br>
                    <label for="about_condotel" class="font-weight-bold" >About condotel   :</label>
                    @if($home != null)
                    <input class="form-control" type="text" placeholder="About condotel" name="about_condotel" required value="{{ old('about_condotel', $home->about_condotel) }}">
                    @else
                    <input class="form-control" type="text" placeholder="About condotel" name="about_condotel" required value="{{ old('about_condotel') }}">
                    @endif
                        @error('about_condotel')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    <br>
                    <div>
                        <label for="image_land" class="font-weight-bold">land Image  :</label>
                        <br>
                        @if($home != null)
                        <img src="{{ asset('storage/'.$home->image_land) }}" class="img-thumbnail" alt="..." style="width: 150px;height: 150px">
                        @else
                        <img src="..." class="img-thumbnail" alt="..." style="width: 150px;height: 150px">
                        @endif
                        <br>
                        <input type="file" name="image_land">
                        @error('image_land')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <br>
                    <label for="about_land" class="font-weight-bold" >About land   :</label>
                    @if($home != null)
                    <input class="form-control" type="text" placeholder="About land" name="about_land" required value="{{ old('about_land', $home->about_land) }}">
                    @else
                    <input class="form-control" type="text" placeholder="About land" name="about_land" required value="{{ old('about_land') }}">
                    @endif
                        @error('about_land')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    <br>
                </div>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <span class="btn btn-danger"><a href="{{ URL::previous() }}">Back</a></span>
                        <button class="btn btn-info ml-1" type="submit">Save</button>
                    </div>
            </form>
        </div>
    </div>

    </div>      

    </main>
    
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- ckeditor -->
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script>
    var konten = document.getElementById("konten");
        CKEDITOR.replace(konten,{
        language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
    </script>

    <script src="{{ asset('ckeditor/plugins/codesnippet/lib/highlight/highlight.pack.js') }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <!-- ckeditor end -->

    <script type="text/javascript">
        $(document).ready(function(){
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
</html>