<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Scrollbar Custom CSS -->
    <link href="{{ asset('style/sidebar/sidebar.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css"> -->
    <!-- csrf token ajax -->
    <title>Document</title>
</head>
<body>
    <main>
<div class="wrapper">

    <!-- Sidebar -->
    <nav id="sidebar">
    @include('component.sidebar')
    </nav>

    <!-- Page Content -->
    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Toggle Sidebar</span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="mt-1">
                            <a href="{{url('/admin')}}">
                                <span>
                                    home
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door-fill" viewBox="0 0 16 16">
                                        <path d="M6.5 14.5v-3.505c0-.245.25-.495.5-.495h2c.25 0 .5.25.5.5v3.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5z"/>
                                    </svg>
                                </span>
                            </a>
                        </li>
                        
                        <li class="ml-3">
                            <p>Tilda Apart</p>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <!-- isi content -->
        <!-- alert success --> 
        @if(!empty($success))
            <div class="alert alert-success" id="modalAlert">
                {{ $success }}
            </div>
        @elseif(session()->has('success'))
            <div class="alert alert-success" id="modalAlert">
                {{ session()->get('success') }}
            </div>
        @endif
        <!-- END::Alert success -->
        @if(count($residences))
        <div class="row">
        @foreach($residences as $residence)
            <div class="col-3">
                <div class="card" style="width: 380px; margin: 5px">
                    @foreach($galleries as $gallery)
                        @if($gallery->property_id == $residence->id)
                        <img src="{{ asset('storage/'.$gallery->image_name) }}" class="card-img-top" alt="..." style="height: 350px; width: 100%">
                        @else
                        @endif
                    @endforeach
                    <div class="card-body">
                        <h5 class="card-title">{!!$residence->name_building!!}</h5>
                        <label for="quote">{!!$residence->quote!!}</label>
                        <p class="card-text">{!!mb_strimwidth(($residence->detail_building),0,25,'....')!!}</p>
                        <div class="row">
                            <div class="col-6">
                                <form action="/admin/residence/destroy/{{ $residence->id }}" method="post">
                                    @method('delete')
                                    @csrf
                                        <button class="btn btn-danger" onclick="return confirm('Are You Sure ?')"><i class="far fa-trash-alt">Delete</i></button>
                                </form>
                            </div>
                            <div class="col-6">
                                <a href="/admin/residence/show/{{$residence->id}}" class="btn btn-primary">Detail</a>
                            </div>
                            <div class="col-6 mt-2">
                                <a href="/admin/residence/image/{{$residence->id}}" class="btn btn-primary">Add image</a>
                            </div>
                            <div class="col-6 mt-2">
                                <a href="/admin/residence/edit/{{$residence->id}}" class="btn btn-primary">Edit</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <h2>No data found!</h2>
        @endif
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <!-- <button class="btn btn-primary me-md-2" type="button">Button</button> -->
            <!-- <button class="btn btn-primary ml-1" type="button"><a href="{{url('/admin/residence/create')}}">Upload</a></button> -->
        </div>
        <!-- isi content -->
        </div>
    </div>
</div>      

    </main>
    
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- ajax -->
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>

</body>
</html>