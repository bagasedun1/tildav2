<!DOCTYPE html>
<html lang="en">
<head>
    @include('component.head')
</head>
<body>

    <header>
        @include('component.header')
    </header>

    <section class="contsection">
        <div class="contact-bg">
            <img src="{{ asset('image/BWPTH_Jakarta_Buildi.jpg') }}" alt="">
        </div>
    </section>

    <section class="contsection">
        <div class="cont-container">
            <div class="cont-field">
                <div class="cont-field-extend left">
                    <div class="cont-img-holder">
                        <img src="image/dawid-sobolewski-359.png" alt="">
                    </div>
                </div>
                <div class="cont-field-extend right">
                    <div class="cont-detail-info">
                        <h1>Send Message</h1>
                        <p>
                            Please feel free to contact us at any time should you have. <br>
                            Any question, concern or request. 
                        </p>
                        @if($contact)
                        <p>email: {{$contact->email}}</p>
                        @else
                        <p>email :</p>
                        @endif
                        <p>
                            We will reply you back within 48 hours <br>
                            if you do not have any replies from us, our replies might be delivered as spam <br>
                            Please see your spam box first or pelase contact us via your another email address <br>
                        </p>
                        <form action="">
                            <input type="email" name="" id="" placeholder="Your Email">
                            <hr>
                            <input type="text" name="" id="" placeholder="Your Full Name">
                            <hr>
                            <input type="text" name="" id="" placeholder="Your Number">
                            <hr>
                            <button>Sign Up</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contsection">
        @include('component.contact')
    </section>

    <footer>
        @include('component.footer')
    </footer>
    
</body>
</html>